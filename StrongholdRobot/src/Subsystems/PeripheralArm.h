#ifndef PeripheralArm_H
#define PeripheralArm_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class PeripheralArm: public Subsystem
{
public:

	PeripheralArm();

	void InitDefaultCommand();

	///@brief Raises the arm.
	void RaiseArm();
	///@brief Lowers the arm.
	void LowerArm();

	///@brief Get whether the arm is open (at max) or not.
	bool IsArmRaised();
	///@brief Get whether the arm is closed (at min) or not.
	bool IsArmLowered();

	///@brief Updates peripheral arm relays.
	void Periodic();
};

#endif
