//StrongHoldRobot v1.0

#include "Launcher.h"
#include "../RobotMap.h"
#include "../Robot.h"

Launcher::Launcher()
	:PIDSubsystem("Launcher", 7.0, 0.0, 8.0)
{
	IsReseting = false;
	SetAbsoluteTolerance(0.005);
	GetPIDController()->SetContinuous(false);

	LiveWindow::GetInstance()->AddActuator("Launcher", "PIDSubsystem Controller", GetPIDController());

	ejectorTimer.Start();
}

void Launcher::InitDefaultCommand()
{

}

void Launcher::SetWheelSpeed(float speed)
{
	RobotMap::flywheels->Set(speed);
}

void Launcher::MoveLauncher(double amount)
{
	if (amount != 0.0)
	{
		Disable();//Disable the PID loop
	}

	RobotMap::launcherTiltMotors->Set(amount * 0.6);
}

void Launcher::SetLauncherAngle(double angle)
{
	SetSetpoint(angle);
}

void Launcher::SetLauncherHigh() {
	SetLauncherAngle(4.889);
}

void Launcher::SetLauncherLow() {
	SetLauncherAngle(4.315);
}

double Launcher::GetLauncherAngle()
{
	return RobotMap::launcherTiltSensor->GetAverageVoltage();
}

bool Launcher::IsLauncherFullyRaised()
{
	//TODO: Check the launcher's max tilt limit switch.
	return false;
}

bool Launcher::IsLauncherFullyLowered()
{
	//TODO: Check the launcher's min tilt limit switch.
	return false;
}

void Launcher::EjectBall()
{
	RobotMap::ejectorMotor->Set(Relay::kReverse);
	ejectorTimer.Reset();
}

void Launcher::ResetEjector()
{
	if (!IsEjectorClosed())
	{
		IsReseting = true;
		RobotMap::ejectorMotor->Set(Relay::kForward);
	}
}

bool Launcher::IsEjectorClosed()
{
	if (!RobotMap::ejectorLimitSwitchMin->Get())
	{
		return true;
	}

	return false;
}

void Launcher::Periodic()
{
	if (RobotMap::ejectorMotor->Get() == Relay::kReverse && ejectorTimer.Get() >= 0.5f) {
		RobotMap::ejectorMotor->StopMotor();
		ResetEjector();
	}

	if (IsReseting) {
		RobotMap::ejectorMotor->Set(Relay::kForward);
	}
	if (IsEjectorClosed()) {
		std::cout << "Ejector Is Closed.\n";
	}
	if (RobotMap::ejectorMotor->Get() == Relay::kForward && IsEjectorClosed())
	{
		RobotMap::ejectorMotor->StopMotor();
		IsReseting = false;
	}
}

double Launcher::ReturnPIDInput()
{
	return RobotMap::launcherTiltSensor->GetAverageVoltage();
}

void Launcher::UsePIDOutput(double output)
{
	RobotMap::launcherTiltMotors->PIDWrite(output);
}
