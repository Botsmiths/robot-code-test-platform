#include "DriveTrain.h"
#include <cmath>
#include "../RobotMap.h"

#include "../Commands/OperatorControl.h"


DriveTrain::DriveTrain()
	:Subsystem("DriveTrain")
{
    leftWheels = RobotMap::driveTrainLeftWheels;
    rightWheels = RobotMap::driveTrainRightWheels;
}

void DriveTrain::InitDefaultCommand()
{
    SetDefaultCommand(new OperatorControl());
}

void DriveTrain::Drive(float forward, float rotate)
{
	float leftMotorOutput = 0.0f;
	float rightMotorOutput = 0.0f;

	if (rotate != 0.0f)//Always move forward slightly to avoid getting stuck
	{
		//forward = 0.1f;
	}

	if (forward > 0.0)
	{
		if (rotate > 0.0)
		{
			leftMotorOutput = forward - rotate;
			rightMotorOutput = std::max(forward, rotate);
		}
		else
		{
			leftMotorOutput = std::max(forward, -rotate);
			rightMotorOutput = forward + rotate;
		}
	}
	else
	{
		if (rotate > 0.0)
		{
			leftMotorOutput = -std::max(-forward, rotate);
			rightMotorOutput = forward + rotate;
		}
		else
		{
			leftMotorOutput = forward - rotate;
			rightMotorOutput = -std::max(-forward, -rotate);
		}
	}

	leftWheels->Set(leftMotorOutput);
	rightWheels->Set(rightMotorOutput);
}

void DriveTrain::Stop()
{
	leftWheels->StopMotor();
	rightWheels->StopMotor();
}



