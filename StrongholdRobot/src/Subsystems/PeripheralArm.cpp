#include "PeripheralArm.h"
#include "../RobotMap.h"

PeripheralArm::PeripheralArm()
	:Subsystem("PeripheralArm")
{

}

void PeripheralArm::InitDefaultCommand()
{

}

void PeripheralArm::RaiseArm()
{
	if (!IsArmRaised())
	{
		RobotMap::peripheralArmMotor->Set(Relay::kForward);
	}
}

void PeripheralArm::LowerArm()
{
	if (!IsArmLowered())
	{
		RobotMap::peripheralArmMotor->Set(Relay::kReverse);
	}
}

bool PeripheralArm::IsArmRaised()
{
	return !RobotMap::peripheralLimitSwitchMax->Get();
}

bool PeripheralArm::IsArmLowered()
{
	return RobotMap::peripheralLimitSwitchMin->Get();
}

void PeripheralArm::Periodic()
{
	//Stop the motor once it reaches the raised position.
	if (RobotMap::peripheralArmMotor->Get() == Relay::kForward && IsArmRaised())
	{
		RobotMap::peripheralArmMotor->StopMotor();
	}
	//Stop the motor once it reaches the lowered position.
	if (RobotMap::peripheralArmMotor->Get() == Relay::kReverse && IsArmLowered())
	{
		RobotMap::peripheralArmMotor->StopMotor();
	}
}
