#include "MovePeripheralArm.h"
#include "../Robot.h"

MovePeripheralArm::MovePeripheralArm(ArmAction action)
	:desiredAction(action),
	 wasRaised(false)
{
	Requires(Robot::peripheralArm.get());
}


void MovePeripheralArm::Initialize()
{
	SetTimeout(5.0f);//Safety expiration.

	if (desiredAction == Toggle)
	{
		if (Robot::peripheralArm->IsArmRaised())
		{
			desiredAction = Lower;
		}
		else
		{
			desiredAction = Raise;
		}
	}

	if (desiredAction == Raise)
	{
		Robot::peripheralArm->RaiseArm();
	}
	else if (desiredAction == Lower)
	{
		Robot::peripheralArm->LowerArm();
	}
}

void MovePeripheralArm::Execute()
{

}

bool MovePeripheralArm::IsFinished()
{
	if (desiredAction == Raise && Robot::peripheralArm->IsArmRaised())
	{
		return true;
	}
	else if (desiredAction == Lower && Robot::peripheralArm->IsArmLowered())
	{
		return true;
	}

	return IsTimedOut();
}

void MovePeripheralArm::End()
{

}

void MovePeripheralArm::Interrupted()
{
	End();
}
