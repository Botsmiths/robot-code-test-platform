#ifndef PickUpBoulders_H
#define PickUpBoulders_H

#include "WPILib.h"

class PickUpBoulders: public Command
{
public:

	PickUpBoulders();

	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
