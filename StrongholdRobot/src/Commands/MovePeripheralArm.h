#ifndef MovePeripheralArm_H
#define MovePeripheralArm_H

#include "WPILib.h"

class MovePeripheralArm: public Command
{
public:

	enum ArmAction
	{
		Lower,
		Raise,
		Toggle
	};

	MovePeripheralArm(ArmAction action = Toggle);

	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:

	ArmAction desiredAction;
	bool wasRaised;
};

#endif
