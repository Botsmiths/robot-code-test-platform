#include "LaunchBoulder.h"
#include "../Robot.h"

LaunchBoulder::LaunchBoulder(float power)
	:LaunchSpeed(power),
	 EjectFlag(false)
{
	Requires(Robot::launcher.get());
}

void LaunchBoulder::Initialize()
{
	EjectFlag = true;

	Robot::launcher->ResetEjector();

	SetTimeout(3.0f); //Wait 2 seconds
}

void LaunchBoulder::Execute()
{
	Robot::launcher->SetWheelSpeed(LaunchSpeed);

	if (TimeSinceInitialized() >= 2.5 && EjectFlag)
	{
		EjectFlag = false;
		Robot::launcher->EjectBall();
	}
}

bool LaunchBoulder::IsFinished()
{
	return IsTimedOut();
}

void LaunchBoulder::End()
{
	Robot::launcher->SetWheelSpeed(0.0f);
}

void LaunchBoulder::Interrupted()
{
	End();//This command just ends when another one takes over.
}
