//StrongHoldRobot v1.0

#ifndef _ROBOT_H
#define _ROBOT_H

#include "WPILib.h"
#include "Commands/Command.h"
#include "RobotMap.h"
#include "LiveWindow/LiveWindow.h"

#include "Subsystems/DriveTrain.h"
#include "Subsystems/Launcher.h"
#include "Subsystems/PeripheralArm.h"
#include "Subsystems/VisionSystem.h"

#include "OI.h"

class Robot : public IterativeRobot
{
public:

	Command *autonomousCommand;

	static bool EmergencyOverride;

	static std::unique_ptr<OI> oi;

	LiveWindow* lw = LiveWindow::GetInstance();
	SendableChooser DefenseSelect;
	SendableChooser ShotSelect;

    static std::shared_ptr<DriveTrain> driveTrain;
    static std::shared_ptr<VisionSystem> visionSystem;
    static std::shared_ptr<Launcher> launcher;
    static std::shared_ptr<PeripheralArm> peripheralArm;

    void CommonPeriodic();

	virtual void RobotInit();
	virtual void DisabledInit();
	virtual void DisabledPeriodic();
	virtual void AutonomousInit();
	virtual void AutonomousPeriodic();
	virtual void TeleopInit();
	virtual void TeleopPeriodic();
	virtual void TestPeriodic();
};
#endif
