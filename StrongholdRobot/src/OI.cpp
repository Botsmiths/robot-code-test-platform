// RobotBuilder Version: 2.0
//
// This file was generated by RobotBuilder. It contains sections of
// code that are automatically generated and assigned by robotbuilder.
// These sections will be updated in the future when you export to
// C++ from RobotBuilder. Do not put any code or make any change in
// the blocks indicating autogenerated code or it will be lost on an
// update. Deleting the comments indicating the section will prevent
// it from being updated in the future.

#include "OI.h"


#include "SmartDashboard/SmartDashboard.h"
#include "Commands/AutonomousCommand.h"
#include "Commands/OperatorControl.h"
#include "Commands/PickUpBoulders.h"
#include "Commands/LaunchBoulder.h"
#include "Commands/CenterTarget.h"
#include "Commands/ResetCannon.h"
#include "Commands/SetLauncherAngle.h"

OI::OI()
{
	//Setup controllers
	float deadzone = 0.15f;
	controller0.reset(new XboxController(0, deadzone));
	controller1.reset(new XboxController(1, deadzone));

	//Map commands to controller buttons

	//controller0->YButton->WhenPressed(new ResetCannon());
	controller1->LeftBumper->WhileHeld(new PickUpBoulders());
	controller1->RightTrigger->WhenPressed(new LaunchBoulder());
	controller1->RightBumper->WhenPressed(new LaunchBoulder(0.65f));

	//controller0->AButton->WhileHeld(new CenterTarget(true));

	//controller1->LeftTrigger->WhileHeld(new SetLauncherAngle(4.817));
	controller1->LeftTrigger->WhileHeld(new CenterTarget());


    // SmartDashboard Buttons
    //SmartDashboard::PutData("Autonomous Command", new AutonomousCommand());
    //SmartDashboard::PutData("Operator Control", new OperatorControl());
}

std::shared_ptr<GameController> OI::GetController(unsigned short num)
{
	if (num == 0)
	{
		return Robot::oi->controller0;
	}
	else
	{
		return Robot::oi->controller1;
	}
}


